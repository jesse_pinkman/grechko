var express = require('express')
var router = express.Router()

router.use('/requests', require('./requests'))
router.use('/persons', require('./persons'))

router.use('/acts', require('./acts'))

module.exports = router
