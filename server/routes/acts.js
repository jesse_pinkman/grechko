let express = require('express')
let router = express.Router()
let models = require('../models')

router.post('/', async function(req, res) {
  let type = req.body.type
  let registratorId = req.body.registratorId

  req.body.type = undefined
  req.body.registratorId = undefined

  let registration = await models.Registration.create({
    startRegistratorId: registratorId,
    endRegistratorId: registratorId,
    startTime: Date.now(),
    endTime: Date.now()
  })

  let act = await models[type].create({ ...req.body, registrationId: registration.id })

  res.status(200).json({ act })
})

router.delete('/:id', async function(req, res) {
  let act = await models[req.query.type].findOne({ where: { id: req.params.id } })

  await act.destroy()

  let registration = await models.Registration.destroy({
    where: {
      id: act.registrationId
    }
  })

  res.status(200).body({ registration, act })
})

module.exports = router
