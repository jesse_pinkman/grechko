let express = require('express')
let router = express.Router()
let models = require('../models')

router.get('/:id/acts', async function(req, res) {
  let id = req.params.id

  let [births, marriages, nameChanges, surnameChanges, adoptions] = await Promise.all([
    models.Birth.findAll({
      where: { personId: id },
      include: [
        {
          model: models.Person,
          include: [
            {
              model: models.SurnameChange
            },
            {
              model: models.NameChange
            }
          ]
        }
      ]
    }),
    models.Marriage.findAll({
      where: { maleId: id },
      include: [
        {
          model: models.Person,
          include: [
            {
              model: models.SurnameChange
            },
            {
              model: models.NameChange
            }
          ]
        }
      ]
    }),
    models.NameChange.findAll({
      where: { personId: id },
      include: [
        {
          model: models.Person,
          include: [
            {
              model: models.SurnameChange
            },
            {
              model: models.NameChange
            }
          ]
        }
      ]
    }),
    models.SurnameChange.findAll({
      where: { personId: id },
      include: [
        {
          model: models.Person,
          include: [
            {
              model: models.SurnameChange
            },
            {
              model: models.NameChange
            }
          ]
        }
      ]
    }),
    models.Adoption.findAll({
      where: { parentId: id },
      include: [
        {
          model: models.Person,
          include: [
            {
              model: models.SurnameChange
            },
            {
              model: models.NameChange
            }
          ]
        }
      ]
    })
  ])

  births = births.map(b => ({ ...b.toJSON(), type: 'Birth' }))
  marriages = marriages.map(b => ({ ...b.toJSON(), type: 'Marriage' }))
  nameChanges = nameChanges.map(b => ({ ...b.toJSON(), type: 'NameChange' }))
  surnameChanges = surnameChanges.map(b => ({ ...b.toJSON(), type: 'SurnameChange' }))
  adoptions = adoptions.map(b => ({ ...b.toJSON(), type: 'Adoption' }))

  return res
    .status(200)
    .json({ acts: [...births, ...marriages, ...nameChanges, ...surnameChanges, ...adoptions] })
})

router.get('/', async function(req, res) {
  let persons = await models.Person.findAll({
    include: [
      {
        model: models.NameChange
      },
      {
        model: models.SurnameChange
      },
      {
        model: models.Birth,
        include: [
          {
            model: models.Place
          }
        ]
      }
    ]
  })
  res.status(200).json({ persons })
})

router.get('/:id', async function(req, res) {
  let person = await models.Person.findOne({
    where: {
      id: req.params.id
    },
    include: [
      {
        model: models.NameChange
      },
      {
        model: models.SurnameChange
      },
      {
        model: models.Birth,
        include: [
          {
            model: models.Place
          }
        ]
      }
    ]
  })
  res.status(200).json({ person })
})

router.post('/', async function(req, res) {
  let {
    date,
    fullName,
    gender,
    isRegistrator,
    city,
    building,
    flat,
    region,
    country,
    registratorId,
    maleParentId,
    femaleParentId
  } = req.body

  let person = await models.Person.create({
    gender,
    isRegistrator,
    maleParentId,
    femaleParentId
  })

  let place = await models.Place.create({ city, building, flat, region, country })

  let registration = await models.Registration.create({
    startTime: date,
    endTime: date,
    startRegistratorId: registratorId,
    endRegistratorId: registratorId
  })

  await Promise.all([
    models.Birth.create({
      placeId: place.id,
      personId: person.id,
      registrationId: registration.id,
      date
    }),
    models.NameChange.create({
      value: fullName.split(' ')[0],
      personId: person.id,
      registrationId: registration.id
    }),
    models.SurnameChange.create({
      value: fullName.split(' ')[1],
      personId: person.id,
      registrationId: registration.id
    })
  ])

  return res.status(200).json({ person })
})

module.exports = router
