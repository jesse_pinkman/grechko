let express = require('express')
let router = express.Router()
let models = require('../models')

router.post('/', async function(req, res) {
  let { date, reason, fullName } = req.body

  let request = await models.Request.create({
    date,
    reason,
    fullName
  })

  return res.status(200).json({ request })
})

router.put('/:id', async function(req, res) {
  let { date, reason, fullName, status, approved } = req.body

  let request = await models.Request.update(
    {
      date,
      reason,
      fullName,
      status,
      approved
    },
    {
      where: {
        id: req.params.id
      }
    }
  )

  return res.status(200).json({ request })
})

router.delete('/:id', async function(req, res) {
  let request = await models.Request.destroy({
    where: {
      id: req.params.id
    }
  })

  return res.status(200).json({ request })
})

router.get('/', async function(req, res) {
  let requests = await models.Request.findAll()

  return res.status(200).json({ requests })
})

router.get('/:id', async function(req, res) {
  let request = await models.Request.findOne({ where: { id: req.params.id } })

  return res.status(200).json({ request })
})

module.exports = router
