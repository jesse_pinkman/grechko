'use strict'

module.exports = (sequelize, DataTypes) => {
  let Person = sequelize.define('Person', {
    gender: DataTypes.STRING,
    isRegistrator: DataTypes.BOOLEAN
  })

  Person.associate = function(models) {
    models.Person.belongsTo(models.Person, {
      foreignKey: 'maleParentId'
    })

    models.Person.belongsTo(models.Person, {
      foreignKey: 'femaleParentId'
    })

    models.Person.hasMany(models.NameChange, {
      foreignKey: 'personId'
    })

    models.Person.hasMany(models.SurnameChange, {
      foreignKey: 'personId'
    })

    models.Person.hasMany(models.Birth, {
      foreignKey: 'personId'
    })
  }

  return Person
}
