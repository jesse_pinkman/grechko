'use strict'

module.exports = (sequelize, DataTypes) => {
  let Request = sequelize.define('Request', {
    date: DataTypes.DATE,
    reason: DataTypes.STRING,
    fullName: DataTypes.STRING,
    approved: { type: DataTypes.BOOLEAN, defaultValue: false },
    status: { type: DataTypes.STRING, defaultValue: 'pending' }
  })

  return Request
}
