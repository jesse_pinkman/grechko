'use strict'

module.exports = (sequelize, DataTypes) => {
  let Place = sequelize.define('Place', {
    country: DataTypes.STRING,
    city: DataTypes.STRING,
    region: DataTypes.STRING,
    flat: DataTypes.STRING,
    building: DataTypes.STRING
  })

  return Place
}
