'use strict'

module.exports = (sequelize, DataTypes) => {
  let Birth = sequelize.define('Birth', {
    date: DataTypes.DATE
  })

  Birth.associate = function(models) {
    models.Birth.belongsTo(models.Person, {
      foreignKey: 'personId'
    })

    models.Birth.belongsTo(models.Place, {
      foreignKey: 'placeId'
    })

    models.Birth.belongsTo(models.Registration, {
      foreignKey: 'registrationId'
    })
  }

  return Birth
}
