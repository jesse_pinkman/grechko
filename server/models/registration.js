'use strict'

module.exports = (sequelize, DataTypes) => {
  let Registration = sequelize.define('Registration', {
    startTime: DataTypes.DATE,
    endTime: DataTypes.DATE
  })

  Registration.associate = function(models) {
    models.Registration.belongsTo(models.Person, {
      foreignKey: 'startRegistratorId'
    })
    models.Registration.belongsTo(models.Person, {
      foreignKey: 'endRegistratorId'
    })
  }

  return Registration
}
