'use strict'

module.exports = (sequelize, DataTypes) => {
  let Adoption = sequelize.define('Adoption')

  Adoption.associate = function(models) {
    models.Adoption.belongsTo(models.Person, {
      foreignKey: 'parentId'
    })

    models.Adoption.belongsTo(models.Person, {
      foreignKey: 'childId'
    })

    models.Adoption.belongsTo(models.Marriage, {
      foreignKey: 'marriageId'
    })

    models.Adoption.belongsTo(models.Registration, {
      foreignKey: 'registrationId'
    })

    models.Adoption.belongsTo(models.NameChange, {
      foreignKey: 'nameChange'
    })
  }

  return Adoption
}
