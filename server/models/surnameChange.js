'use strict'

module.exports = (sequelize, DataTypes) => {
  let SurnameChange = sequelize.define('SurnameChange', {
    value: DataTypes.STRING
  })

  SurnameChange.associate = function(models) {
    models.SurnameChange.belongsTo(models.Person, {
      foreignKey: 'personId'
    })

    models.SurnameChange.belongsTo(models.Registration, {
      foreignKey: 'registrationId'
    })
  }

  return SurnameChange
}
