'use strict'

module.exports = (sequelize, DataTypes) => {
  let Marriage = sequelize.define('Marriage')

  Marriage.associate = function(models) {
    models.Marriage.belongsTo(models.Person, {
      foreignKey: 'maleId'
    })

    models.Marriage.belongsTo(models.Person, {
      foreignKey: 'femaleId'
    })

    models.Marriage.belongsTo(models.Registration, {
      foreignKey: 'registrationId'
    })

    models.Marriage.belongsTo(models.SurnameChange, {
      foreignKey: 'surnameChangeId'
    })
  }

  return Marriage
}
