'use strict'

module.exports = (sequelize, DataTypes) => {
  let NameChange = sequelize.define('NameChange', {
    value: DataTypes.STRING
  })

  NameChange.associate = function(models) {
    models.NameChange.belongsTo(models.Person, {
      foreignKey: 'personId'
    })

    models.NameChange.belongsTo(models.Registration, {
      foreignKey: 'registrationId'
    })
  }

  return NameChange
}
